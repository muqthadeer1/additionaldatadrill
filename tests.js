const countAllPeople = require('./countAllPeople');
countAllPeople();

const everyOne= require('./everyOne');
everyOne();

const nameWithA = require('./nameWithA');
nameWithA();

const nameWithS = require('./nameWithS');
nameWithS ();

const peopleByHouses = require('./peopleByHouses');
peopleByHouses();

const peopleNameOfAllHouses = require('./peopleNameOfAllHouses');
peopleNameOfAllHouses();

const surnameWithA = require('./surnameWithA');
surnameWithA();

const surnameWithS = require('./nameWithS');
surnameWithS();
const gotData = require('./data.js');

 function countAllPeople(){
let totalPeoples = 0;

Object.entries(gotData).forEach(([house,houseDetails])=>{

     houseDetails.forEach((eachHouseDetails)=>{
        let eachPeople = eachHouseDetails.people
        totalPeoples += eachPeople.length;
     })
   
})

console.log(totalPeoples);
 }

 module.exports = countAllPeople;

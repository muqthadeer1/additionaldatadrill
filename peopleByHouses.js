const gotData = require('./data.js');

function peopleByHouses(){
let objectHouse = {}
Object.entries(gotData).forEach(([house,housesDetails])=>{
    housesDetails.forEach((eachDetails)=>{
        objectHouse[eachDetails.name] = eachDetails.people.length;
    })
});

console.log(objectHouse);
}


module.exports = peopleByHouses;
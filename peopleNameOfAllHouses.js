const gotData = require('./data.js');

function peopleNameOfAllHouses(){
const finalResult = {}
Object.entries(gotData).forEach(([house,housesdetails])=>{

    housesdetails.forEach((eachHouseName)=>{
        eachHouseName.people.forEach((element)=>{
            if(finalResult[eachHouseName.name]){
                finalResult[eachHouseName.name].push(element.name);
            }else{
                finalResult[eachHouseName.name] =[element.name];
            }
            
        })
    })
    console.log(finalResult);

})
}

module.exports = peopleNameOfAllHouses;
